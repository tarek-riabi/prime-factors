package com.example;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsCalculator {

    public Integer[] getFactors(Integer i) {
        return listToArray(findTwoFactors(i));
    }

    private boolean isPrime(Integer i) {
        for (int j = 2; j < i; j++) {
            if ((i/j) * j == i) {
                return false;
            }
        }
        return true;
    }

    private List<Integer> findTwoFactors(Integer i) {
        var temp = new ArrayList<Integer>();
        temp.add(1);
        for (int j = 2;j < i; j++) {
            int div = i / j;
            if (div * j == i) {
                if (isPrime(j) && !temp.contains(j)) {
                    temp.add(j);
                }
                if (isPrime(div) && !temp.contains(div)) {
                    temp.add(div);
                }
            }
        }
        if (isPrime(i) && !temp.contains(i)) {
            temp.add(i);
        }
        return temp;
    }

    private Integer[] listToArray(List<Integer> list) {
        return list.toArray(new Integer[0]);
    }
}
