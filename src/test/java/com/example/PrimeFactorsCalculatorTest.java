package com.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PrimeFactorsCalculatorTest {

    PrimeFactorsCalculator primeFactorsCalculator;

    @BeforeEach
    public void init() {
        primeFactorsCalculator = new PrimeFactorsCalculator();
    }

    @Test
    public void should_return_1_given_one() {
        assertArrayEquals(new Integer[]{1}, primeFactorsCalculator.getFactors(1));
    }

    @Test
    public void should_return_1_2_given_two() {
        assertArrayEquals(new Integer[]{1, 2}, primeFactorsCalculator.getFactors(2));
    }

    @Test
    public void should_return_1_3_given_3() {
        assertArrayEquals(new Integer[]{1, 3}, primeFactorsCalculator.getFactors(3));
    }

    @Test
    public void should_return_1_2_given_4() {
        assertArrayEquals(new Integer[]{1, 2}, primeFactorsCalculator.getFactors(4));
    }

    @Test
    public void should_return_1_5_given_5() {
        assertArrayEquals(new Integer[]{1, 5}, primeFactorsCalculator.getFactors(5));
    }

    @Test
    public void should_return_1_2_3_given_6() {
        assertArrayEquals(new Integer[]{1, 2, 3}, primeFactorsCalculator.getFactors(6));
    }

    @Test
    public void should_return_1_7_given_7() {
        assertArrayEquals(new Integer[]{1, 7}, primeFactorsCalculator.getFactors(7));
    }

    @Test
    public void should_return_1_2_given_8() {
        assertArrayEquals(new Integer[]{1, 2}, primeFactorsCalculator.getFactors(8));
    }

    @Test
    public void should_return_1_3_given_9() {
        assertArrayEquals(new Integer[]{1, 3}, primeFactorsCalculator.getFactors(9));
    }

    @Test
    public void should_return_1_2_5_given_10() {
        assertArrayEquals(new Integer[]{1, 2, 5}, primeFactorsCalculator.getFactors(10));
    }

    @Test
    public void should_return_1_11_given_11() {
        assertArrayEquals(new Integer[]{1, 11}, primeFactorsCalculator.getFactors(11));
    }

    @Test
    public void should_return_1_2_3_given_12() {
        assertArrayEquals(new Integer[]{1, 2, 3}, primeFactorsCalculator.getFactors(12));
    }

    @Test
    public void should_return_1_13_given_13() {
        assertArrayEquals(new Integer[]{1, 13}, primeFactorsCalculator.getFactors(13));
    }

    @Test
    public void should_return_1_2_7_given_14() {
        assertArrayEquals(new Integer[]{1, 2, 7}, primeFactorsCalculator.getFactors(14));
    }

    @Test
    public void should_return_1_3_5_given_15() {
        assertArrayEquals(new Integer[]{1, 3, 5}, primeFactorsCalculator.getFactors(15));
    }

    @Test
    public void should_return_1_2_given_16() {
        assertArrayEquals(new Integer[]{1, 2}, primeFactorsCalculator.getFactors(16));
    }

    @Test
    public void should_return_1_2_3_given_18() {
        assertArrayEquals(new Integer[]{1, 2, 3}, primeFactorsCalculator.getFactors(18));
    }

    @Test
    public void should_return_1_2_5_given_20() {
        assertArrayEquals(new Integer[]{1, 2, 5}, primeFactorsCalculator.getFactors(20));
    }

    @Test
    public void should_return_1_3_7_given_21() {
        assertArrayEquals(new Integer[]{1, 3, 7}, primeFactorsCalculator.getFactors(21));
    }

    @Test
    public void should_return_1_2_11_given_22() {
        assertArrayEquals(new Integer[]{1, 2, 11}, primeFactorsCalculator.getFactors(22));
    }

    @Test
    public void should_return_1_2_3_given_24() {
        assertArrayEquals(new Integer[]{1, 2, 3}, primeFactorsCalculator.getFactors(24));
    }

    @Test
    public void should_return_1_2_13_given_26() {
        assertArrayEquals(new Integer[]{1, 2, 13}, primeFactorsCalculator.getFactors(26));
    }

    @Test
    public void should_return_1_2_7_given_28() {
        assertArrayEquals(new Integer[]{1, 2, 7}, primeFactorsCalculator.getFactors(28));
    }

    @Test
    public void should_return_1_2_3_5_given_30() {
        assertArrayEquals(new Integer[]{1, 2, 3, 5}, primeFactorsCalculator.getFactors(30));
    }

    @Test
    public void should_return_1_2_3_5_7_11_13_given_30030() {
        assertArrayEquals(new Integer[]{1, 2, 3, 5, 7, 11, 13}, primeFactorsCalculator.getFactors(30030));
    }

    @Test
    public void should_return_1_23_29_31_given_475571() {
        assertArrayEquals(new Integer[]{1, 23, 29, 31}, primeFactorsCalculator.getFactors(475571));
    }
}
